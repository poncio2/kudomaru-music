
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');

window.Vue = require('vue');





/**
 * Next, we will register the base admin page components
 *
 *
 */

Vue.component('admin-page', require('./components/admin/AdminPage.vue'));
Vue.component('menu-items', require('./components/admin/MenuItems.vue'));
Vue.component('menu-tabs', require('./components/admin/MenuTabs.vue'));

/**
 * Next, we will register the sessions admin page components
 *
 *
 */

Vue.component('admin-sessions', require('./components/admin/sessions/AdminSessions.vue'));
Vue.component('all-sessions-list', require('./components/admin/sessions/AllSessionsList.vue'));
Vue.component('new-session-form', require('./components/admin/sessions/NewSessionForm.vue'));
Vue.component('track-list', require('./components/admin/sessions/TrackList.vue'));
Vue.component('track-list-row', require('./components/admin/sessions/TrackListRow.vue'));

/**
 * Next, we will register the genres admin page components
 *
 *
 */

 Vue.component('admin-genres', require('./components/admin/genres/AdminGenres.vue'));
 Vue.component('all-genres-list', require('./components/admin/genres/AllGenresList.vue'));
 Vue.component('new-genre-form', require('./components/admin/genres/NewGenreForm.vue'));

 /**
  * Next, we will register the artists admin page components
  *
  *
  */

  Vue.component('admin-artists', require('./components/admin/artists/AdminArtists.vue'));
  Vue.component('all-artists-list', require('./components/admin/artists/AllArtistsList.vue'));
  Vue.component('new-artist-form', require('./components/admin/artists/NewArtistForm.vue'));


  /**
   * Next, we will register the images admin components
   *
   *
   */

   Vue.component('admin-images', require('./components/admin/images/AdminImages.vue'));
   Vue.component('all-images-list', require('./components/admin/images/AllImagesList.vue'));
   Vue.component('image-uploader', require('./components/admin/images/ImageUploader.vue'));

const app = new Vue({
    el: '#app'
});
