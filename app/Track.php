<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'title'
  ];

  /**
   *
   * Gets the genre for this track.
   *
   */
  public function genre()
  {
      return $this->belogsTo('App\Genre');
  }


  /**
   *
   * Gets the artist this track belongs to.
   *
   */
  public function artist()
  {
      return $this->belogsTo('App\Artist');
  }




}
