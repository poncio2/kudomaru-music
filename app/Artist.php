<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{


  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name'
  ];

  /**
   *
   * Gets the tracks that belong to this artist.
   *
   */
  public function tracks()
  {
      return $this->hasMany('App\Track');
  }


}
